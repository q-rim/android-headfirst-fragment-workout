package com.example.kyurim.workout;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class TempActivity extends AppCompatActivity {

    final static String TAG = "---TempActiivty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        // inflate Fragement on FrameLayout
        if (savedInstanceState == null) {            // TempActivity has been created before, hence no need to add new instance
            StopwatchFragment stopwatchFragment = new StopwatchFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container_stopwatch, stopwatchFragment);  // arg1: layout;  arg2: java
            fragmentTransaction.addToBackStack(null);                                       // stack it so we can use "back button"
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.commit();
        }
    }
}
