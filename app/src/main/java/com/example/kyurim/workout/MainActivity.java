package com.example.kyurim.workout;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements WorkoutListFragment.Listener {

    private final static String TAG = "---MainActivity:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void itemClicked(int id) {
        View fragmentContainer = findViewById(R.id.fragment_container);
        if (fragmentContainer != null) {            // framgent_container layout only exists in layout-large
            Log.i(TAG, "Device: Tablet");

            // Add the fragment to the FrameLayout
            WorkoutDetailFragment detailFragment = new WorkoutDetailFragment();         // create My Fragment Object
            detailFragment.setWorkoutId(id);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();    // begin Transaction
            ft.replace(R.id.fragment_container, detailFragment);                        // replace Fragment
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);                // get new and old Fragments to fade in and out
            ft.addToBackStack(null);                                                    // add Transaction to the backStack
            ft.commit();                                                                // commit Transaction

        } else {
            Log.i(TAG, "Device: Phone");
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.EXTRA_WORKOUT_ID, id);
            startActivity(intent);
        }
    }
}
