package com.example.kyurim.workout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutDetailFragment extends Fragment {

    private final static String TAG = "---WorkoutDetailFrag:";
    private int workoutId;                                     // this is the ID of workout user chooses

    public WorkoutDetailFragment() {
        // Required empty public constructor
        Log.i(TAG, "constructed");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("workoutId", workoutId);          // saving Fragment State
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inflate Fragement on FrameLayout
        if (savedInstanceState == null) {            // TempActivity has been created before, hence no need to add new instance
            StopwatchFragment stopwatchFragment = new StopwatchFragment();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container_stopwatch, stopwatchFragment);  // arg1: layout;  arg2: java
            fragmentTransaction.addToBackStack(null);                                       // stack it so we can use "back button"
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.commit();
        } else {                            // restoring Fragment state from screen rotation
            workoutId = savedInstanceState.getInt("workoutId");     // restore the workout value.
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,           // LayoutInflator - used to inflate Fragment's Layout
                             ViewGroup container,               // ViewGroup - is the Activity's layout that will contain this Fragment
                             Bundle savedInstanceState) {       // Bundle - used if you've saved the Fragment's state and reinstate it.
        Log.i(TAG, "onCreateView()");
        return inflater.inflate(R.layout.fragment_workout_detail, container, false);    // Inflate the layout for this fragment
    }

    @Override
    public void onStart() {         // when Fragment Starts, set the View
        Log.i(TAG, "onStart()");
        super.onStart();

        View view = getView();      // getView() method gets fragment's root View. - used to get reference to workout title & description.
        if (view != null) {
            // TextView - set title
            TextView title = (TextView) view.findViewById(R.id.textView_workoutTitle);
            Workout workout = Workout.workouts[workoutId];
            title.setText(workout.getName());

            // TextView - set description
            TextView description = (TextView) view.findViewById(R.id.textView_workoutDescription);
            description.setText(workout.getDescription());
        }
    }

    public void setWorkoutId(int id) {
        this.workoutId = id;
    }
}
