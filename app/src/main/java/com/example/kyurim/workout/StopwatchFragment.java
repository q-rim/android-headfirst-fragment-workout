package com.example.kyurim.workout;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class StopwatchFragment extends Fragment implements View.OnClickListener {

    final static String TAG = "---StopwatchFragment";
    private int seconds = 0;
    private boolean running;
    private boolean wasRunning;

    public StopwatchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        Log.i(TAG, "+ onAttach() - unique to Fragment");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "+ onCreate()");
        super.onCreate(savedInstanceState);

        // restoring saved instance state
        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds");
            running = savedInstanceState.getBoolean("running");
            wasRunning = savedInstanceState.getBoolean("wasRunning");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "+ onCreateView() - unique to Fragment");
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        runTimer(layout);   // set fragment's layout and start the runTimer() method, passing in the layout.

        // set listener for Buttons
        Button startButton = (Button) layout.findViewById(R.id.button_start);
        startButton.setOnClickListener(this);
        Button stopButton = (Button) layout.findViewById(R.id.button_stop);
        stopButton.setOnClickListener(this);
        Button resetButton = (Button) layout.findViewById(R.id.button_reset);
        resetButton.setOnClickListener(this);

        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, "+ onActivityCreated() - unique to Fragment");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "+ onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "+ onResume()");
        super.onResume();

        if (wasRunning) {
            running = true;         // if stopwatch was running before it was paused, set it running again.
        }
    }

    @Override
    public void onPause() {
        Log.i(TAG, "- onPause()");
        super.onPause();

        wasRunning = running;       // if fragment is paused, record whether the stopwatch was running and stop it.
        running = false;
    }

    @Override
    public void onStop() {
        Log.i(TAG, "- onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "- onDestroyView() - unique to Fragment");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "- onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "- onDetach() - unique to Fragment");
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {        // saving States before onDestroy()
        savedInstanceState.putInt("seconds", seconds);
        savedInstanceState.putBoolean("running", running);
        savedInstanceState.putBoolean("wasRunning", wasRunning);
    }

    private void onClickStart() {           // Button calls
        Log.i(TAG, "# onClickStart()");
        running = true;                     // needs to run when user clicks on Start Button
    }

    private void onClickStop() {            // Button calls
        Log.i(TAG, "# onClickStop()");
        running = false;                    // needs to run when user clicks on Stop Button
    }

    private void onClickReset() {           // Button calls
        Log.i(TAG, "# onClickReset()");
        running = false;                    // needs to run when user clicks on Reset Button
        seconds = 0;
    }

    @Override
    public void onClick(View view) {        // this method handles all the onClick from Button calls
        switch (view.getId()) {             // get reference to item clicked
            case R.id.button_start:
                onClickStart();
                break;
            case R.id.button_stop:
                onClickStop();
                break;
            case R.id.button_reset:
                onClickReset();
                break;
        }
    }

    private void runTimer(View view) {
        final TextView timeView = (TextView) view.findViewById(R.id.textView_timeView);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                int hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int sec = seconds % 60;

                String time = String.format(Locale.getDefault(), "%d:%02d:%02d", hours, minutes, sec);
                timeView.setText(time);

                if (running) {
                    seconds++;
                }
                handler.postDelayed(this, 1000);
            }
        });
    }
}
